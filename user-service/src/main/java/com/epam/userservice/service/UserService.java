package com.epam.userservice.service;

import com.epam.userservice.bean.AppUserDTO;
import com.epam.userservice.util.CustomResponse;

import java.util.List;

public interface UserService {

    CustomResponse<List<AppUserDTO>> getAllUsers();

    CustomResponse<AppUserDTO> getUser(Long userId);

    CustomResponse<AppUserDTO> saveUser(AppUserDTO UserDTO);

    CustomResponse<AppUserDTO> deleteUser(Long userId);

    CustomResponse<AppUserDTO> updateUser(Long userId, AppUserDTO UserDTO);
}
