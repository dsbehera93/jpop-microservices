package com.epam.bookservice.service;

import com.epam.bookservice.bean.BookDTO;
import com.epam.bookservice.util.CustomResponse;

import java.util.List;

public interface BookService {

    CustomResponse<List<BookDTO>> getAllBooks();

    CustomResponse<BookDTO> getBook(Long bookId);

    CustomResponse<BookDTO> saveBook(BookDTO bookDTO);

    CustomResponse<BookDTO> deleteBook(Long bookId);

    CustomResponse<BookDTO> updateBook(Long bookId, BookDTO bookDTO);
}
