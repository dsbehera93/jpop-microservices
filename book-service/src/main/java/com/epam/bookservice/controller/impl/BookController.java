package com.epam.bookservice.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.epam.bookservice.bean.BookDTO;
import com.epam.bookservice.controller.BookApi;
import com.epam.bookservice.service.BookService;
import com.epam.bookservice.util.CustomResponse;

@RestController
public class BookController implements BookApi {

    @Autowired
    private BookService bookService;

    @Override
    public CustomResponse<List<BookDTO>> getAllBooks() {
        return bookService.getAllBooks();
    }

    @Override
    public CustomResponse<BookDTO> getBook(Long bookId) {
        return bookService.getBook(bookId);
    }

    @Override
    public CustomResponse<BookDTO> saveBook(BookDTO bookDTO) {
        return bookService.saveBook(bookDTO);
    }

    @Override
    public CustomResponse<BookDTO> deleteBook(Long bookId) {
        return bookService.deleteBook(bookId);
    }

    @Override
    public CustomResponse<BookDTO> updateBook(Long bookId, BookDTO bookDTO) {
        return bookService.updateBook(bookId, bookDTO);
    }

}
