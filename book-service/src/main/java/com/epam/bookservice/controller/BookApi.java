package com.epam.bookservice.controller;

import com.epam.bookservice.bean.BookDTO;
import com.epam.bookservice.util.CustomResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/books")
public interface BookApi {

    @GetMapping
    CustomResponse<List<BookDTO>> getAllBooks();

    @GetMapping(path = "/{bookId}")
    CustomResponse<BookDTO> getBook(@PathVariable Long bookId);

    @PostMapping
    CustomResponse<BookDTO> saveBook(@RequestBody BookDTO bookDTO);

    @DeleteMapping(path = "/{bookId}")
    CustomResponse<BookDTO> deleteBook(@PathVariable Long bookId);

    @PutMapping(path = "/{bookId}")
    CustomResponse<BookDTO> updateBook(@PathVariable Long bookId, @RequestBody BookDTO bookDTO);
}
