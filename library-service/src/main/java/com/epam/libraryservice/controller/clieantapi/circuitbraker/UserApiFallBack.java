package com.epam.libraryservice.controller.clieantapi.circuitbraker;

import org.springframework.stereotype.Component;

import com.epam.libraryservice.controller.clieantapi.UserClientApi;
import com.epam.libraryservice.util.CustomResponse;

@Component
public class UserApiFallBack implements UserClientApi {

    @Override
    public Object getAllUsers() {
        return getDefaultFallBackResponse();
    }

    @Override
    public Object getUser(Long userId) {
        return getDefaultFallBackResponse();
    }

    private CustomResponse getDefaultFallBackResponse() {
        return new CustomResponse(false, "User service is down / we are not able to communicate !!!");
    }
}
