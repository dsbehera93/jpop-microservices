package com.epam.libraryservice.controller;

import com.epam.libraryservice.bean.LibraryDTO;
import com.epam.libraryservice.util.CustomResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/libraries")
public interface LibraryApi {

    @GetMapping
    CustomResponse<List<LibraryDTO>> getAllLibraries();

    @GetMapping(path = "/{libraryId}")
    CustomResponse<LibraryDTO> getLibrary(@PathVariable Long libraryId);

    @PostMapping
    CustomResponse<LibraryDTO> saveLibrary(@RequestBody LibraryDTO libraryDTO);

    @DeleteMapping(path = "/{libraryId}")
    CustomResponse<LibraryDTO> deleteLibrary(@PathVariable Long libraryId);

    @PutMapping(path = "/{libraryId}")
    CustomResponse<LibraryDTO> updateLibrary(@PathVariable Long libraryId, @RequestBody LibraryDTO libraryDTO);
}
