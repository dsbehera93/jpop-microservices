package com.epam.libraryservice.controller.clieantapi.circuitbraker;

import org.springframework.stereotype.Component;

import com.epam.libraryservice.controller.clieantapi.BookClientApi;
import com.epam.libraryservice.util.CustomResponse;

@Component
public class BookApiFallBack implements BookClientApi {

    @Override
    public Object getAllBooks() {
        return getDefaultFallBackResponse();
    }

    @Override
    public Object getBook(Long bookId) {
        return getDefaultFallBackResponse();
    }

    private CustomResponse getDefaultFallBackResponse() {
        return new CustomResponse(false, "Book service is down / we are not able to communicate !!!");
    }
}
