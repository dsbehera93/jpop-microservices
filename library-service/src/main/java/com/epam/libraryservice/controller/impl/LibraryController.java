package com.epam.libraryservice.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.epam.libraryservice.bean.LibraryDTO;
import com.epam.libraryservice.controller.LibraryApi;
import com.epam.libraryservice.service.LibraryService;
import com.epam.libraryservice.util.CustomResponse;

@RestController
public class LibraryController implements LibraryApi {

    @Autowired
    private LibraryService libraryService;

    @Override
    public CustomResponse<List<LibraryDTO>> getAllLibraries() {
        return libraryService.getAllLibraries();
    }

    @Override
    public CustomResponse<LibraryDTO> getLibrary(Long libraryId) {
        return libraryService.getLibrary(libraryId);
    }

    @Override
    public CustomResponse<LibraryDTO> saveLibrary(LibraryDTO libraryDTO) {
        return libraryService.saveLibrary(libraryDTO);
    }

    @Override
    public CustomResponse<LibraryDTO> deleteLibrary(Long libraryId) {
        return libraryService.deleteLibrary(libraryId);
    }

    @Override
    public CustomResponse<LibraryDTO> updateLibrary(Long libraryId, LibraryDTO libraryDTO) {
        return libraryService.updateLibrary(libraryId, libraryDTO);
    }

}
