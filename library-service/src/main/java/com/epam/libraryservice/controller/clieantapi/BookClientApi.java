package com.epam.libraryservice.controller.clieantapi;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.epam.libraryservice.controller.clieantapi.circuitbraker.BookApiFallBack;

@FeignClient(name = "${client.server.config.books}", fallback = BookApiFallBack.class)
public interface BookClientApi {

    @GetMapping(path = "/books")
    Object getAllBooks();

    @GetMapping(path = "/books/{bookId}")
    Object getBook(@PathVariable Long bookId);
}
