package com.epam.libraryservice.controller.clieantapi;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.epam.libraryservice.controller.clieantapi.circuitbraker.UserApiFallBack;

@FeignClient(name = "${client.server.config.users}", fallback = UserApiFallBack.class)
public interface UserClientApi {

    @GetMapping(path = "/users")
    Object getAllUsers();

    @GetMapping(path = "/users/{userId}")
    Object getUser(@PathVariable Long userId);
}
